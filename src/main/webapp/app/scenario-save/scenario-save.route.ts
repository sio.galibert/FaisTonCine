import { Injectable } from '@angular/core';
import { Route } from '@angular/router';

import { ScenarioSaveComponent } from './scenario-save.component';

export const scenarioMgmtRoute: Route = {
    path: 'scenario-save',
    component: ScenarioSaveComponent
};
