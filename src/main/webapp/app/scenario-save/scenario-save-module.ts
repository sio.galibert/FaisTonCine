import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FaisTonCineSharedModule } from '../shared';

import { scenarioMgmtRoute } from './scenario-save.route';
import { ScenarioSaveComponent } from './scenario-save.component';
import { DndModule } from 'ng2-dnd';

@NgModule({
    imports: [
        FaisTonCineSharedModule,
        RouterModule.forChild([ scenarioMgmtRoute ]),
        DndModule.forRoot()
    ],
    declarations: [
        ScenarioSaveComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FaisTonCineScenarioSaveModule {}
