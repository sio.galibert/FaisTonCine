import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthServerProvider } from '../shared/auth/auth-jwt.service';

@Component({
  selector: 'jhi-scenario-save',
  templateUrl: './scenario-save.component.html',
  styleUrls: [
    'scenario-save.css'
  ]
})
export class ScenarioSaveComponent implements OnInit {

  ideasList = [];
    // {
    //   id: 1,
    //   title: 'Java',
    //   gender: 'programmation',
    //   synopsys: 'tototototto o ekj odjs oiji jijidsj idj dij',
    //   keywords: ['java', 'vcafé'],
    //   roles: ['scenariste', 'acteur']
    // },
    // {
    //   id: 2,
    //   title: 'C++',
    //   gender: 'programmation',
    //   synopsys: 'tototototto o ekj odjs oiji jijidsj idj dij',
    //   keywords: ['java', 'café'],
    //   roles: ['scenariste', 'acteur']
    // },
    // {
    //   id: 3,
    //   title: 'C#',
    //   gender: 'programmation',
    //   synopsys: 'tototototto o ekj odjs oiji jijidsj idj dij',
    //   keywords: ['java', 'café'],
    //   roles: ['scenariste', 'acteur']
    // }
  // ];

  personnasList = [];

  receivedData = [];

  constructor(private http: HttpClient, private authServerProvider: AuthServerProvider) {  }

  ngOnInit() {
    this.getAllidea();
    this.getPersonna();
  }

  getAllidea = () => {
    const token = 'Bearer ' + this.authServerProvider.getToken();

    const header = new HttpHeaders().set('Authorization', token);
    const options = {
      headers: header,
    };

    this.http.get('/api/ideas', options).subscribe((data: any) => {
      this.ideasList = data;
    });

  }

  getPersonna = () => {
    const token = 'Bearer ' + this.authServerProvider.getToken();

    const header = new HttpHeaders().set('Authorization', token);
    const options = {
      headers: header,
    };

    this.http.get('/api/personas', options).subscribe((data: any) => {
      this.personnasList = data;
    });

  }

  transitionData(originTab, targetTab, object) {
    const pos = originTab.map(function(e) { return e.id; }).indexOf(object.id);
    originTab.splice(pos, 1);
    targetTab.push(object);
  }

  transferDataSuccess($event: any) {
    const object = $event.dragData;
    this.transitionData(this.ideasList, this.receivedData, object);
  }

  revert(object: any) {
    this.transitionData(this.receivedData, this.ideasList, object);
  }

}
