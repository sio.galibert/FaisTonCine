import './vendor.ts';

import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2Webstorage, LocalStorageService, SessionStorageService  } from 'ngx-webstorage';
import { JhiEventManager } from 'ng-jhipster';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { FaisTonCineSharedModule, UserRouteAccessService } from './shared';
import { FaisTonCineAppRoutingModule} from './app-routing.module';
import { FaisTonCineHomeModule } from './home/home.module';
import { FaisTonCineAdminModule } from './admin/admin.module';
import { FaisTonCineAccountModule } from './account/account.module';
import { FaisTonCineEntityModule } from './entities/entity.module';
import { FaisTonCineIdeaSaveModule } from './idea-save/idea-save.module';
import { FaisTonCineScenarioSaveModule } from './scenario-save/scenario-save-module';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import {
    JhiMainComponent,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ErrorComponent
} from './layouts';
// import { ScenarioSaveComponent } from './scenario-save/scenario-save.component';
// import { IdeaSaveComponent } from './idea-save/idea-save.component';

@NgModule({
    imports: [
        BrowserModule,
        FaisTonCineAppRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        FaisTonCineSharedModule,
        FaisTonCineHomeModule,
        FaisTonCineAdminModule,
        FaisTonCineAccountModule,
        FaisTonCineEntityModule,
        FaisTonCineIdeaSaveModule,
        FaisTonCineScenarioSaveModule,
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        FooterComponent,
        // ScenarioSaveComponent,
        // IdeaSaveComponent
    ],
    providers: [
        ProfileService,
        PaginationConfig,
        UserRouteAccessService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true,
            deps: [
                LocalStorageService,
                SessionStorageService
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthExpiredInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorHandlerInterceptor,
            multi: true,
            deps: [
                JhiEventManager
            ]
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NotificationInterceptor,
            multi: true,
            deps: [
                Injector
            ]
        }
    ],
    bootstrap: [ JhiMainComponent ]
})
export class FaisTonCineAppModule {}
