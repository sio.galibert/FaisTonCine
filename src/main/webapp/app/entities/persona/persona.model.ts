import { BaseEntity } from './../../shared';

export class Persona implements BaseEntity {
    constructor(
        public id?: string,
        public firstname?: string,
        public name?: string,
        public pseudo?: string,
        public sexe?: boolean,
        public age?: number,
        public bio?: string,
        public caractere?: string,
    ) {
        this.sexe = false;
    }
}
