import { BaseEntity } from './../../shared';

export class Ideas implements BaseEntity {
    constructor(
        public id?: string,
        public titre?: string,
        public genre?: string,
        public emotions?: string,
        public phraseSynopsis?: string,
        public keywords?: string,
        public roles?: string,
    ) {
    }
}
