import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Ideas } from './ideas.model';
import { IdeasPopupService } from './ideas-popup.service';
import { IdeasService } from './ideas.service';

@Component({
    selector: 'jhi-ideas-delete-dialog',
    templateUrl: './ideas-delete-dialog.component.html'
})
export class IdeasDeleteDialogComponent {

    ideas: Ideas;

    constructor(
        private ideasService: IdeasService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.ideasService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ideasListModification',
                content: 'Deleted an ideas'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ideas-delete-popup',
    template: ''
})
export class IdeasDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ideasPopupService: IdeasPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ideasPopupService
                .open(IdeasDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
