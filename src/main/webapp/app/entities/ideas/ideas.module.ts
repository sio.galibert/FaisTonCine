import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FaisTonCineSharedModule } from '../../shared';
import {
    IdeasService,
    IdeasPopupService,
    IdeasComponent,
    IdeasDetailComponent,
    IdeasDialogComponent,
    IdeasPopupComponent,
    IdeasDeletePopupComponent,
    IdeasDeleteDialogComponent,
    ideasRoute,
    ideasPopupRoute,
} from './';

const ENTITY_STATES = [
    ...ideasRoute,
    ...ideasPopupRoute,
];

@NgModule({
    imports: [
        FaisTonCineSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        IdeasComponent,
        IdeasDetailComponent,
        IdeasDialogComponent,
        IdeasDeleteDialogComponent,
        IdeasPopupComponent,
        IdeasDeletePopupComponent,
    ],
    entryComponents: [
        IdeasComponent,
        IdeasDialogComponent,
        IdeasPopupComponent,
        IdeasDeleteDialogComponent,
        IdeasDeletePopupComponent,
    ],
    providers: [
        IdeasService,
        IdeasPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FaisTonCineIdeasModule {}
