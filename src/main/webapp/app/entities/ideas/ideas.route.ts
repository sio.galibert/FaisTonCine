import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { IdeasComponent } from './ideas.component';
import { IdeasDetailComponent } from './ideas-detail.component';
import { IdeasPopupComponent } from './ideas-dialog.component';
import { IdeasDeletePopupComponent } from './ideas-delete-dialog.component';

export const ideasRoute: Routes = [
    {
        path: 'ideas',
        component: IdeasComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ideas'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ideas/:id',
        component: IdeasDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ideas'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ideasPopupRoute: Routes = [
    {
        path: 'ideas-new',
        component: IdeasPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ideas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ideas/:id/edit',
        component: IdeasPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ideas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ideas/:id/delete',
        component: IdeasDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ideas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
