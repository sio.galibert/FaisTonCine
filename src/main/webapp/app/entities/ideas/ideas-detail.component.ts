import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Ideas } from './ideas.model';
import { IdeasService } from './ideas.service';

@Component({
    selector: 'jhi-ideas-detail',
    templateUrl: './ideas-detail.component.html'
})
export class IdeasDetailComponent implements OnInit, OnDestroy {

    ideas: Ideas;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ideasService: IdeasService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInIdeas();
    }

    load(id) {
        this.ideasService.find(id)
            .subscribe((ideasResponse: HttpResponse<Ideas>) => {
                this.ideas = ideasResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInIdeas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ideasListModification',
            (response) => this.load(this.ideas.id)
        );
    }
}
