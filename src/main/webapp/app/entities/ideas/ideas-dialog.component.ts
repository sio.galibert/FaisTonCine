import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Ideas } from './ideas.model';
import { IdeasPopupService } from './ideas-popup.service';
import { IdeasService } from './ideas.service';

@Component({
    selector: 'jhi-ideas-dialog',
    templateUrl: './ideas-dialog.component.html'
})
export class IdeasDialogComponent implements OnInit {

    ideas: Ideas;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private ideasService: IdeasService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ideas.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ideasService.update(this.ideas));
        } else {
            this.subscribeToSaveResponse(
                this.ideasService.create(this.ideas));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Ideas>>) {
        result.subscribe((res: HttpResponse<Ideas>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Ideas) {
        this.eventManager.broadcast({ name: 'ideasListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-ideas-popup',
    template: ''
})
export class IdeasPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ideasPopupService: IdeasPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ideasPopupService
                    .open(IdeasDialogComponent as Component, params['id']);
            } else {
                this.ideasPopupService
                    .open(IdeasDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
