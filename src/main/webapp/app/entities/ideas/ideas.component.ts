import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Ideas } from './ideas.model';
import { IdeasService } from './ideas.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-ideas',
    templateUrl: './ideas.component.html'
})
export class IdeasComponent implements OnInit, OnDestroy {
ideas: Ideas[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ideasService: IdeasService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.ideasService.query().subscribe(
            (res: HttpResponse<Ideas[]>) => {
                this.ideas = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInIdeas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Ideas) {
        return item.id;
    }
    registerChangeInIdeas() {
        this.eventSubscriber = this.eventManager.subscribe('ideasListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
