import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Ideas } from './ideas.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Ideas>;

@Injectable()
export class IdeasService {

    private resourceUrl =  SERVER_API_URL + 'api/ideas';

    constructor(private http: HttpClient) { }

    create(ideas: Ideas): Observable<EntityResponseType> {
        const copy = this.convert(ideas);
        return this.http.post<Ideas>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(ideas: Ideas): Observable<EntityResponseType> {
        const copy = this.convert(ideas);
        return this.http.put<Ideas>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<Ideas>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Ideas[]>> {
        const options = createRequestOption(req);
        return this.http.get<Ideas[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Ideas[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Ideas = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Ideas[]>): HttpResponse<Ideas[]> {
        const jsonResponse: Ideas[] = res.body;
        const body: Ideas[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Ideas.
     */
    private convertItemFromServer(ideas: Ideas): Ideas {
        const copy: Ideas = Object.assign({}, ideas);
        return copy;
    }

    /**
     * Convert a Ideas to a JSON which can be sent to the server.
     */
    private convert(ideas: Ideas): Ideas {
        const copy: Ideas = Object.assign({}, ideas);
        return copy;
    }
}
