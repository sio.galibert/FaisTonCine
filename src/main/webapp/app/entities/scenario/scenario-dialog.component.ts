import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Scenario } from './scenario.model';
import { ScenarioPopupService } from './scenario-popup.service';
import { ScenarioService } from './scenario.service';

@Component({
    selector: 'jhi-scenario-dialog',
    templateUrl: './scenario-dialog.component.html'
})
export class ScenarioDialogComponent implements OnInit {

    scenario: Scenario;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private scenarioService: ScenarioService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.scenario.id !== undefined) {
            this.subscribeToSaveResponse(
                this.scenarioService.update(this.scenario));
        } else {
            this.subscribeToSaveResponse(
                this.scenarioService.create(this.scenario));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Scenario>>) {
        result.subscribe((res: HttpResponse<Scenario>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Scenario) {
        this.eventManager.broadcast({ name: 'scenarioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-scenario-popup',
    template: ''
})
export class ScenarioPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private scenarioPopupService: ScenarioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.scenarioPopupService
                    .open(ScenarioDialogComponent as Component, params['id']);
            } else {
                this.scenarioPopupService
                    .open(ScenarioDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
