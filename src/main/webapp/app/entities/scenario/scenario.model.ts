import { BaseEntity } from './../../shared';

export class Scenario implements BaseEntity {
    constructor(
        public id?: string,
        public idee?: string,
        public scenario?: string,
        public genre?: string,
    ) {
    }
}
