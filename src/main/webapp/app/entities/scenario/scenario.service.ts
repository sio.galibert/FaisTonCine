import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Scenario } from './scenario.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Scenario>;

@Injectable()
export class ScenarioService {

    private resourceUrl =  SERVER_API_URL + 'api/scenarios';

    constructor(private http: HttpClient) { }

    create(scenario: Scenario): Observable<EntityResponseType> {
        const copy = this.convert(scenario);
        return this.http.post<Scenario>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(scenario: Scenario): Observable<EntityResponseType> {
        const copy = this.convert(scenario);
        return this.http.put<Scenario>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<Scenario>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Scenario[]>> {
        const options = createRequestOption(req);
        return this.http.get<Scenario[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Scenario[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Scenario = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Scenario[]>): HttpResponse<Scenario[]> {
        const jsonResponse: Scenario[] = res.body;
        const body: Scenario[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Scenario.
     */
    private convertItemFromServer(scenario: Scenario): Scenario {
        const copy: Scenario = Object.assign({}, scenario);
        return copy;
    }

    /**
     * Convert a Scenario to a JSON which can be sent to the server.
     */
    private convert(scenario: Scenario): Scenario {
        const copy: Scenario = Object.assign({}, scenario);
        return copy;
    }
}
