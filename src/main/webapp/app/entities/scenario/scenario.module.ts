import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FaisTonCineSharedModule } from '../../shared';
import {
    ScenarioService,
    ScenarioPopupService,
    ScenarioComponent,
    ScenarioDetailComponent,
    ScenarioDialogComponent,
    ScenarioPopupComponent,
    ScenarioDeletePopupComponent,
    ScenarioDeleteDialogComponent,
    scenarioRoute,
    scenarioPopupRoute,
} from './';

const ENTITY_STATES = [
    ...scenarioRoute,
    ...scenarioPopupRoute,
];

@NgModule({
    imports: [
        FaisTonCineSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ScenarioComponent,
        ScenarioDetailComponent,
        ScenarioDialogComponent,
        ScenarioDeleteDialogComponent,
        ScenarioPopupComponent,
        ScenarioDeletePopupComponent,
    ],
    entryComponents: [
        ScenarioComponent,
        ScenarioDialogComponent,
        ScenarioPopupComponent,
        ScenarioDeleteDialogComponent,
        ScenarioDeletePopupComponent,
    ],
    providers: [
        ScenarioService,
        ScenarioPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FaisTonCineScenarioModule {}
