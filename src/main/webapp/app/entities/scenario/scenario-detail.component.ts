import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Scenario } from './scenario.model';
import { ScenarioService } from './scenario.service';

@Component({
    selector: 'jhi-scenario-detail',
    templateUrl: './scenario-detail.component.html'
})
export class ScenarioDetailComponent implements OnInit, OnDestroy {

    scenario: Scenario;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private scenarioService: ScenarioService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInScenarios();
    }

    load(id) {
        this.scenarioService.find(id)
            .subscribe((scenarioResponse: HttpResponse<Scenario>) => {
                this.scenario = scenarioResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInScenarios() {
        this.eventSubscriber = this.eventManager.subscribe(
            'scenarioListModification',
            (response) => this.load(this.scenario.id)
        );
    }
}
