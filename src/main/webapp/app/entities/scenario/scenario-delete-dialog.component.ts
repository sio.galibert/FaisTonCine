import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Scenario } from './scenario.model';
import { ScenarioPopupService } from './scenario-popup.service';
import { ScenarioService } from './scenario.service';

@Component({
    selector: 'jhi-scenario-delete-dialog',
    templateUrl: './scenario-delete-dialog.component.html'
})
export class ScenarioDeleteDialogComponent {

    scenario: Scenario;

    constructor(
        private scenarioService: ScenarioService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.scenarioService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'scenarioListModification',
                content: 'Deleted an scenario'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-scenario-delete-popup',
    template: ''
})
export class ScenarioDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private scenarioPopupService: ScenarioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.scenarioPopupService
                .open(ScenarioDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
