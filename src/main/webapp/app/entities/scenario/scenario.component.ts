import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Scenario } from './scenario.model';
import { ScenarioService } from './scenario.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-scenario',
    templateUrl: './scenario.component.html'
})
export class ScenarioComponent implements OnInit, OnDestroy {
scenarios: Scenario[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private scenarioService: ScenarioService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.scenarioService.query().subscribe(
            (res: HttpResponse<Scenario[]>) => {
                this.scenarios = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInScenarios();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Scenario) {
        return item.id;
    }
    registerChangeInScenarios() {
        this.eventSubscriber = this.eventManager.subscribe('scenarioListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
