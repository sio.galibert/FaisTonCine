import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ScenarioComponent } from './scenario.component';
import { ScenarioDetailComponent } from './scenario-detail.component';
import { ScenarioPopupComponent } from './scenario-dialog.component';
import { ScenarioDeletePopupComponent } from './scenario-delete-dialog.component';

export const scenarioRoute: Routes = [
    {
        path: 'scenario',
        component: ScenarioComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Scenarios'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'scenario/:id',
        component: ScenarioDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Scenarios'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const scenarioPopupRoute: Routes = [
    {
        path: 'scenario-new',
        component: ScenarioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Scenarios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'scenario/:id/edit',
        component: ScenarioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Scenarios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'scenario/:id/delete',
        component: ScenarioDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Scenarios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
