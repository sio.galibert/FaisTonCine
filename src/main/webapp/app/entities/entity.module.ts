import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { FaisTonCineIdeasModule } from './ideas/ideas.module';
import { FaisTonCinePersonaModule } from './persona/persona.module';
import { FaisTonCineScenarioModule } from './scenario/scenario.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        FaisTonCineIdeasModule,
        FaisTonCinePersonaModule,
        FaisTonCineScenarioModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FaisTonCineEntityModule {}
