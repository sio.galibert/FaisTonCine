import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FaisTonCineSharedModule } from '../shared';

import { ideaMgmtRoute } from './idea-save.route';
import { IdeaSaveComponent } from './idea-save.component';

@NgModule({
    imports: [
        FaisTonCineSharedModule,
        RouterModule.forChild([ ideaMgmtRoute ])
    ],
    declarations: [
        IdeaSaveComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FaisTonCineIdeaSaveModule {}
