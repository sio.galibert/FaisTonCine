import { Injectable } from '@angular/core';
import { Route } from '@angular/router';

import { IdeaSaveComponent } from './idea-save.component';

export const ideaMgmtRoute: Route = {
    path: 'idea-save',
    component: IdeaSaveComponent
};
