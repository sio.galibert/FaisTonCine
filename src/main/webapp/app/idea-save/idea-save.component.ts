import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthServerProvider } from '../shared/auth/auth-jwt.service';

@Component({
  selector: 'jhi-idea-save',
  templateUrl: './idea-save.component.html',
  styleUrls: [
    'idea-save.css'
  ]
})
export class IdeaSaveComponent implements OnInit {

  newIdea = { titre: null, genre: null, phraseSynopsis: null, emotions: null, keywords: null, roles: null };

  synopsisList = [{ synopsis: '', add: false }];

  fileToUpload: File = null;
  emotionList = [];

  newKeyword = '';
  keywordList = [];

  newRole = '';
  roleList = [];

  kind = 'action';
  resultScenario = '';

  constructor(private http: HttpClient, private authServerProvider: AuthServerProvider) { }

  ngOnInit() {
  }

  addSynopsis = (synopsis: any) => {
    const idx = this.synopsisList.indexOf(synopsis);
    this.synopsisList.splice(idx, 1, { synopsis: synopsis.synopsis, add: true });
    this.synopsisList.push({ synopsis: '', add: false });
  }

  removeSynopsis = (synopsis: any, index: number) => {
    this.synopsisList.splice(index, 1);
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();

    const self = this;

    myReader.onloadend = (e) => {
      const base64img = myReader.result.split([','])[1];
      const json = { requests: [{ image: { content: base64img }, features: [{ type: 'FACE_DETECTION', maxResults: 10 }] }] };
      const header = new HttpHeaders().set('Content-Type', 'application/json');
      const options = {
        headers: header
      };

      self.http.post('https://vision.googleapis.com/v1/images:annotate?key=AIzaSyCtcDBqCy-Sm9m4EkROPxFwIVVpCJmtHFg', json, options).subscribe((data: any) => {
        if (!data.hasOwnProperty('error')) {
          // console.log(data);
          data.responses[0].faceAnnotations.forEach((face) => {
            const emotions = {
              surprise: face.surpriseLikelihood,
              headwear: face.headwearLikelihood,
              joy: face.joyLikelihood,
              sorrow: face.sorrowLikelihood,
              anger: face.angerLikelihood,
              blurred: face.blurredLikelihood,
              underExposed: face.underExposedLikelihood
            };
            // console.log(emotions);
            Object.keys(emotions).forEach(function(key) {
              // console.log(key, emotions[key]);
              if ((emotions[key] === 'VERY_LIKELY' || emotions[key] === 'LIKELY') && !self.emotionList.includes(key)) {
                // console.log(self.emotionList.indexOf(key));
                self.emotionList.push(key);
                console.log(key);
              }
            });
          });
        }
      });
    };
    myReader.readAsDataURL(file);
  }

  removeEmotion = (index: number) => {
    this.emotionList.splice(index, 1);
  }

  addKeyword = () => {
    this.keywordList.push({ keyword: this.newKeyword });
  }

  removeKeyword = (keyword: any, index: number) => {
    this.keywordList.splice(index, 1);
  }

  addRole = () => {
    this.roleList.push({ role: this.newRole });
  }

  removeRole = (role: any, index: number) => {
    this.roleList.splice(index, 1);
  }

  addIdea = () => {
    this.newIdea.phraseSynopsis = JSON.stringify(this.synopsisList);
    this.newIdea.emotions = JSON.stringify(this.emotionList);
    this.newIdea.keywords = JSON.stringify(this.keywordList);
    this.newIdea.roles = JSON.stringify(this.roleList);
    console.log(this.newIdea);

    const json = this.newIdea;

    const token = 'Bearer ' + this.authServerProvider.getToken();

    const header = new HttpHeaders().set('Authorization', token);
    const options = {
      headers: header,
    };

    this.http.post('/api/ideas', json, options).subscribe((data) => {
      console.log('envoyer');
    });
  }

  generateScenario() {
    console.log(this.kind);
    this.http.get('/api/generateScenario/' + this.kind).subscribe((data: any) => {
      console.log(data);
      this.resultScenario = data.response;
    });
  }
}
