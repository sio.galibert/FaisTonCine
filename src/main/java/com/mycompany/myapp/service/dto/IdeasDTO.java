package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Ideas entity.
 */
public class IdeasDTO implements Serializable {

    private String id;

    private String titre;

    private String genre;

    private String emotions;

    private String phraseSynopsis;

    private String keywords;

    private String roles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getEmotions() {
        return emotions;
    }

    public void setEmotions(String emotions) {
        this.emotions = emotions;
    }

    public String getPhraseSynopsis() {
        return phraseSynopsis;
    }

    public void setPhraseSynopsis(String phraseSynopsis) {
        this.phraseSynopsis = phraseSynopsis;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IdeasDTO ideasDTO = (IdeasDTO) o;
        if(ideasDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ideasDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IdeasDTO{" +
            "id=" + getId() +
            ", titre='" + getTitre() + "'" +
            ", genre='" + getGenre() + "'" +
            ", emotions='" + getEmotions() + "'" +
            ", phraseSynopsis='" + getPhraseSynopsis() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", roles='" + getRoles() + "'" +
            "}";
    }
}
