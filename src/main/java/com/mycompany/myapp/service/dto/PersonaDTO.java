package com.mycompany.myapp.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Persona entity.
 */
public class PersonaDTO implements Serializable {

    private String id;

    @Size(min = 1)
    private String firstname;

    @Size(min = 1)
    private String name;

    @Size(min = 1)
    private String pseudo;

    private Boolean sexe;

    @Min(value = 0)
    private Integer age;

    @Size(min = 1)
    private String bio;

    private String caractere;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Boolean isSexe() {
        return sexe;
    }

    public void setSexe(Boolean sexe) {
        this.sexe = sexe;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCaractere() {
        return caractere;
    }

    public void setCaractere(String caractere) {
        this.caractere = caractere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PersonaDTO personaDTO = (PersonaDTO) o;
        if(personaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), personaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PersonaDTO{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", name='" + getName() + "'" +
            ", pseudo='" + getPseudo() + "'" +
            ", sexe='" + isSexe() + "'" +
            ", age=" + getAge() +
            ", bio='" + getBio() + "'" +
            ", caractere='" + getCaractere() + "'" +
            "}";
    }
}
