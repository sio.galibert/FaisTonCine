package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Scenario entity.
 */
public class ScenarioDTO implements Serializable {

    private String id;

    private String idee;

    private String scenario;

    private String genre;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdee() {
        return idee;
    }

    public void setIdee(String idee) {
        this.idee = idee;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScenarioDTO scenarioDTO = (ScenarioDTO) o;
        if(scenarioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), scenarioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ScenarioDTO{" +
            "id=" + getId() +
            ", idee='" + getIdee() + "'" +
            ", scenario='" + getScenario() + "'" +
            ", genre='" + getGenre() + "'" +
            "}";
    }
}
