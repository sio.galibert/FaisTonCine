package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.IdeasDTO;
import java.util.List;

/**
 * Service Interface for managing Ideas.
 */
public interface IdeasService {

    /**
     * Save a ideas.
     *
     * @param ideasDTO the entity to save
     * @return the persisted entity
     */
    IdeasDTO save(IdeasDTO ideasDTO);

    /**
     * Get all the ideas.
     *
     * @return the list of entities
     */
    List<IdeasDTO> findAll();

    /**
     * Get the "id" ideas.
     *
     * @param id the id of the entity
     * @return the entity
     */
    IdeasDTO findOne(String id);

    /**
     * Delete the "id" ideas.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
