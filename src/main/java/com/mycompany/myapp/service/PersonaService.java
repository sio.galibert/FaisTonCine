package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.PersonaDTO;
import java.util.List;

/**
 * Service Interface for managing Persona.
 */
public interface PersonaService {

    /**
     * Save a persona.
     *
     * @param personaDTO the entity to save
     * @return the persisted entity
     */
    PersonaDTO save(PersonaDTO personaDTO);

    /**
     * Get all the personas.
     *
     * @return the list of entities
     */
    List<PersonaDTO> findAll();

    /**
     * Get the "id" persona.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PersonaDTO findOne(String id);

    /**
     * Delete the "id" persona.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
