package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.IdeasDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Ideas and its DTO IdeasDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IdeasMapper extends EntityMapper<IdeasDTO, Ideas> {


}
