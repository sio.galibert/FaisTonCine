package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.ScenarioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Scenario and its DTO ScenarioDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ScenarioMapper extends EntityMapper<ScenarioDTO, Scenario> {


}
