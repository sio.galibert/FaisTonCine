package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.IdeasService;
import com.mycompany.myapp.domain.Ideas;
import com.mycompany.myapp.repository.IdeasRepository;
import com.mycompany.myapp.service.dto.IdeasDTO;
import com.mycompany.myapp.service.mapper.IdeasMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Ideas.
 */
@Service
public class IdeasServiceImpl implements IdeasService {

    private final Logger log = LoggerFactory.getLogger(IdeasServiceImpl.class);

    private final IdeasRepository ideasRepository;

    private final IdeasMapper ideasMapper;

    public IdeasServiceImpl(IdeasRepository ideasRepository, IdeasMapper ideasMapper) {
        this.ideasRepository = ideasRepository;
        this.ideasMapper = ideasMapper;
    }

    /**
     * Save a ideas.
     *
     * @param ideasDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public IdeasDTO save(IdeasDTO ideasDTO) {
        log.debug("Request to save Ideas : {}", ideasDTO);
        Ideas ideas = ideasMapper.toEntity(ideasDTO);
        ideas = ideasRepository.save(ideas);
        return ideasMapper.toDto(ideas);
    }

    /**
     * Get all the ideas.
     *
     * @return the list of entities
     */
    @Override
    public List<IdeasDTO> findAll() {
        log.debug("Request to get all Ideas");
        return ideasRepository.findAll().stream()
            .map(ideasMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one ideas by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public IdeasDTO findOne(String id) {
        log.debug("Request to get Ideas : {}", id);
        Ideas ideas = ideasRepository.findOne(id);
        return ideasMapper.toDto(ideas);
    }

    /**
     * Delete the ideas by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Ideas : {}", id);
        ideasRepository.delete(id);
    }
}
