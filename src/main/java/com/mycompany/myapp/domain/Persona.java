package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Persona.
 */
@Document(collection = "persona")
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Size(min = 1)
    @Field("firstname")
    private String firstname;

    @Size(min = 1)
    @Field("name")
    private String name;

    @Size(min = 1)
    @Field("pseudo")
    private String pseudo;

    @Field("sexe")
    private Boolean sexe;

    @Min(value = 0)
    @Field("age")
    private Integer age;

    @Size(min = 1)
    @Field("bio")
    private String bio;

    @Field("caractere")
    private String caractere;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public Persona firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getName() {
        return name;
    }

    public Persona name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPseudo() {
        return pseudo;
    }

    public Persona pseudo(String pseudo) {
        this.pseudo = pseudo;
        return this;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public Boolean isSexe() {
        return sexe;
    }

    public Persona sexe(Boolean sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(Boolean sexe) {
        this.sexe = sexe;
    }

    public Integer getAge() {
        return age;
    }

    public Persona age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBio() {
        return bio;
    }

    public Persona bio(String bio) {
        this.bio = bio;
        return this;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCaractere() {
        return caractere;
    }

    public Persona caractere(String caractere) {
        this.caractere = caractere;
        return this;
    }

    public void setCaractere(String caractere) {
        this.caractere = caractere;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Persona persona = (Persona) o;
        if (persona.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), persona.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Persona{" +
            "id=" + getId() +
            ", firstname='" + getFirstname() + "'" +
            ", name='" + getName() + "'" +
            ", pseudo='" + getPseudo() + "'" +
            ", sexe='" + isSexe() + "'" +
            ", age=" + getAge() +
            ", bio='" + getBio() + "'" +
            ", caractere='" + getCaractere() + "'" +
            "}";
    }
}
