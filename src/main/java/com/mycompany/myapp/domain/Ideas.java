package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Ideas.
 */
@Document(collection = "ideas")
public class Ideas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("titre")
    private String titre;

    @Field("genre")
    private String genre;

    @Field("emotions")
    private String emotions;

    @Field("phraseSynopsis")
    private String phraseSynopsis;

    @Field("keywords")
    private String keywords;

    @Field("roles")
    private String roles;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public Ideas titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getGenre() {
        return genre;
    }

    public Ideas genre(String genre) {
        this.genre = genre;
        return this;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getEmotions() {
        return emotions;
    }

    public Ideas emotions(String emotions) {
        this.emotions = emotions;
        return this;
    }

    public void setEmotions(String emotions) {
        this.emotions = emotions;
    }

    public String getPhraseSynopsis() {
        return phraseSynopsis;
    }

    public Ideas phraseSynopsis(String phraseSynopsis) {
        this.phraseSynopsis = phraseSynopsis;
        return this;
    }

    public void setPhraseSynopsis(String phraseSynopsis) {
        this.phraseSynopsis = phraseSynopsis;
    }

    public String getKeywords() {
        return keywords;
    }

    public Ideas keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getRoles() {
        return roles;
    }

    public Ideas roles(String roles) {
        this.roles = roles;
        return this;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ideas ideas = (Ideas) o;
        if (ideas.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ideas.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Ideas{" +
            "id=" + getId() +
            ", titre='" + getTitre() + "'" +
            ", genre='" + getGenre() + "'" +
            ", emotions='" + getEmotions() + "'" +
            ", phraseSynopsis='" + getPhraseSynopsis() + "'" +
            ", keywords='" + getKeywords() + "'" +
            ", roles='" + getRoles() + "'" +
            "}";
    }
}
