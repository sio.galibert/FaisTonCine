package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Scenario.
 */
@Document(collection = "scenario")
public class Scenario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("idee")
    private String idee;

    @Field("scenario")
    private String scenario;

    @Field("genre")
    private String genre;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdee() {
        return idee;
    }

    public Scenario idee(String idee) {
        this.idee = idee;
        return this;
    }

    public void setIdee(String idee) {
        this.idee = idee;
    }

    public String getScenario() {
        return scenario;
    }

    public Scenario scenario(String scenario) {
        this.scenario = scenario;
        return this;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getGenre() {
        return genre;
    }

    public Scenario genre(String genre) {
        this.genre = genre;
        return this;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Scenario scenario = (Scenario) o;
        if (scenario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), scenario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Scenario{" +
            "id=" + getId() +
            ", idee='" + getIdee() + "'" +
            ", scenario='" + getScenario() + "'" +
            ", genre='" + getGenre() + "'" +
            "}";
    }
}
