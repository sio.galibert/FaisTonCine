package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Scenario;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Scenario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ScenarioRepository extends MongoRepository<Scenario, String> {

}
