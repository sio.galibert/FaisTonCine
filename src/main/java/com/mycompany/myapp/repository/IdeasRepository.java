package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Ideas;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Ideas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IdeasRepository extends MongoRepository<Ideas, String> {

}
