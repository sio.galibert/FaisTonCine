/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FaisTonCineTestModule } from '../../../test.module';
import { IdeasComponent } from '../../../../../../main/webapp/app/entities/ideas/ideas.component';
import { IdeasService } from '../../../../../../main/webapp/app/entities/ideas/ideas.service';
import { Ideas } from '../../../../../../main/webapp/app/entities/ideas/ideas.model';

describe('Component Tests', () => {

    describe('Ideas Management Component', () => {
        let comp: IdeasComponent;
        let fixture: ComponentFixture<IdeasComponent>;
        let service: IdeasService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FaisTonCineTestModule],
                declarations: [IdeasComponent],
                providers: [
                    IdeasService
                ]
            })
            .overrideTemplate(IdeasComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdeasComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdeasService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Ideas('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.ideas[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
