/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { FaisTonCineTestModule } from '../../../test.module';
import { IdeasDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/ideas/ideas-delete-dialog.component';
import { IdeasService } from '../../../../../../main/webapp/app/entities/ideas/ideas.service';

describe('Component Tests', () => {

    describe('Ideas Management Delete Component', () => {
        let comp: IdeasDeleteDialogComponent;
        let fixture: ComponentFixture<IdeasDeleteDialogComponent>;
        let service: IdeasService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FaisTonCineTestModule],
                declarations: [IdeasDeleteDialogComponent],
                providers: [
                    IdeasService
                ]
            })
            .overrideTemplate(IdeasDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdeasDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdeasService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
