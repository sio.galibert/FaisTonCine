/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { FaisTonCineTestModule } from '../../../test.module';
import { IdeasDetailComponent } from '../../../../../../main/webapp/app/entities/ideas/ideas-detail.component';
import { IdeasService } from '../../../../../../main/webapp/app/entities/ideas/ideas.service';
import { Ideas } from '../../../../../../main/webapp/app/entities/ideas/ideas.model';

describe('Component Tests', () => {

    describe('Ideas Management Detail Component', () => {
        let comp: IdeasDetailComponent;
        let fixture: ComponentFixture<IdeasDetailComponent>;
        let service: IdeasService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FaisTonCineTestModule],
                declarations: [IdeasDetailComponent],
                providers: [
                    IdeasService
                ]
            })
            .overrideTemplate(IdeasDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdeasDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdeasService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Ideas('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.ideas).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
