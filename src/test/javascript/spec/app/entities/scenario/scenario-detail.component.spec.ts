/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { FaisTonCineTestModule } from '../../../test.module';
import { ScenarioDetailComponent } from '../../../../../../main/webapp/app/entities/scenario/scenario-detail.component';
import { ScenarioService } from '../../../../../../main/webapp/app/entities/scenario/scenario.service';
import { Scenario } from '../../../../../../main/webapp/app/entities/scenario/scenario.model';

describe('Component Tests', () => {

    describe('Scenario Management Detail Component', () => {
        let comp: ScenarioDetailComponent;
        let fixture: ComponentFixture<ScenarioDetailComponent>;
        let service: ScenarioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FaisTonCineTestModule],
                declarations: [ScenarioDetailComponent],
                providers: [
                    ScenarioService
                ]
            })
            .overrideTemplate(ScenarioDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ScenarioDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ScenarioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Scenario('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.scenario).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
