/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { FaisTonCineTestModule } from '../../../test.module';
import { ScenarioComponent } from '../../../../../../main/webapp/app/entities/scenario/scenario.component';
import { ScenarioService } from '../../../../../../main/webapp/app/entities/scenario/scenario.service';
import { Scenario } from '../../../../../../main/webapp/app/entities/scenario/scenario.model';

describe('Component Tests', () => {

    describe('Scenario Management Component', () => {
        let comp: ScenarioComponent;
        let fixture: ComponentFixture<ScenarioComponent>;
        let service: ScenarioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [FaisTonCineTestModule],
                declarations: [ScenarioComponent],
                providers: [
                    ScenarioService
                ]
            })
            .overrideTemplate(ScenarioComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ScenarioComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ScenarioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Scenario('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.scenarios[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
