package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.FaisTonCineApp;

import com.mycompany.myapp.domain.Ideas;
import com.mycompany.myapp.repository.IdeasRepository;
import com.mycompany.myapp.service.IdeasService;
import com.mycompany.myapp.service.dto.IdeasDTO;
import com.mycompany.myapp.service.mapper.IdeasMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the IdeasResource REST controller.
 *
 * @see IdeasResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaisTonCineApp.class)
public class IdeasResourceIntTest {

    private static final String DEFAULT_TITRE = "AAAAAAAAAA";
    private static final String UPDATED_TITRE = "BBBBBBBBBB";

    private static final String DEFAULT_GENRE = "AAAAAAAAAA";
    private static final String UPDATED_GENRE = "BBBBBBBBBB";

    private static final String DEFAULT_EMOTIONS = "AAAAAAAAAA";
    private static final String UPDATED_EMOTIONS = "BBBBBBBBBB";

    private static final String DEFAULT_PHRASE_SYNOPSIS = "AAAAAAAAAA";
    private static final String UPDATED_PHRASE_SYNOPSIS = "BBBBBBBBBB";

    private static final String DEFAULT_KEYWORDS = "AAAAAAAAAA";
    private static final String UPDATED_KEYWORDS = "BBBBBBBBBB";

    private static final String DEFAULT_ROLES = "AAAAAAAAAA";
    private static final String UPDATED_ROLES = "BBBBBBBBBB";

    @Autowired
    private IdeasRepository ideasRepository;

    @Autowired
    private IdeasMapper ideasMapper;

    @Autowired
    private IdeasService ideasService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restIdeasMockMvc;

    private Ideas ideas;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IdeasResource ideasResource = new IdeasResource(ideasService);
        this.restIdeasMockMvc = MockMvcBuilders.standaloneSetup(ideasResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ideas createEntity() {
        Ideas ideas = new Ideas()
            .titre(DEFAULT_TITRE)
            .genre(DEFAULT_GENRE)
            .emotions(DEFAULT_EMOTIONS)
            .phraseSynopsis(DEFAULT_PHRASE_SYNOPSIS)
            .keywords(DEFAULT_KEYWORDS)
            .roles(DEFAULT_ROLES);
        return ideas;
    }

    @Before
    public void initTest() {
        ideasRepository.deleteAll();
        ideas = createEntity();
    }

    @Test
    public void createIdeas() throws Exception {
        int databaseSizeBeforeCreate = ideasRepository.findAll().size();

        // Create the Ideas
        IdeasDTO ideasDTO = ideasMapper.toDto(ideas);
        restIdeasMockMvc.perform(post("/api/ideas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ideasDTO)))
            .andExpect(status().isCreated());

        // Validate the Ideas in the database
        List<Ideas> ideasList = ideasRepository.findAll();
        assertThat(ideasList).hasSize(databaseSizeBeforeCreate + 1);
        Ideas testIdeas = ideasList.get(ideasList.size() - 1);
        assertThat(testIdeas.getTitre()).isEqualTo(DEFAULT_TITRE);
        assertThat(testIdeas.getGenre()).isEqualTo(DEFAULT_GENRE);
        assertThat(testIdeas.getEmotions()).isEqualTo(DEFAULT_EMOTIONS);
        assertThat(testIdeas.getPhraseSynopsis()).isEqualTo(DEFAULT_PHRASE_SYNOPSIS);
        assertThat(testIdeas.getKeywords()).isEqualTo(DEFAULT_KEYWORDS);
        assertThat(testIdeas.getRoles()).isEqualTo(DEFAULT_ROLES);
    }

    @Test
    public void createIdeasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ideasRepository.findAll().size();

        // Create the Ideas with an existing ID
        ideas.setId("existing_id");
        IdeasDTO ideasDTO = ideasMapper.toDto(ideas);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIdeasMockMvc.perform(post("/api/ideas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ideasDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Ideas in the database
        List<Ideas> ideasList = ideasRepository.findAll();
        assertThat(ideasList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllIdeas() throws Exception {
        // Initialize the database
        ideasRepository.save(ideas);

        // Get all the ideasList
        restIdeasMockMvc.perform(get("/api/ideas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ideas.getId())))
            .andExpect(jsonPath("$.[*].titre").value(hasItem(DEFAULT_TITRE.toString())))
            .andExpect(jsonPath("$.[*].genre").value(hasItem(DEFAULT_GENRE.toString())))
            .andExpect(jsonPath("$.[*].emotions").value(hasItem(DEFAULT_EMOTIONS.toString())))
            .andExpect(jsonPath("$.[*].phraseSynopsis").value(hasItem(DEFAULT_PHRASE_SYNOPSIS.toString())))
            .andExpect(jsonPath("$.[*].keywords").value(hasItem(DEFAULT_KEYWORDS.toString())))
            .andExpect(jsonPath("$.[*].roles").value(hasItem(DEFAULT_ROLES.toString())));
    }

    @Test
    public void getIdeas() throws Exception {
        // Initialize the database
        ideasRepository.save(ideas);

        // Get the ideas
        restIdeasMockMvc.perform(get("/api/ideas/{id}", ideas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ideas.getId()))
            .andExpect(jsonPath("$.titre").value(DEFAULT_TITRE.toString()))
            .andExpect(jsonPath("$.genre").value(DEFAULT_GENRE.toString()))
            .andExpect(jsonPath("$.emotions").value(DEFAULT_EMOTIONS.toString()))
            .andExpect(jsonPath("$.phraseSynopsis").value(DEFAULT_PHRASE_SYNOPSIS.toString()))
            .andExpect(jsonPath("$.keywords").value(DEFAULT_KEYWORDS.toString()))
            .andExpect(jsonPath("$.roles").value(DEFAULT_ROLES.toString()));
    }

    @Test
    public void getNonExistingIdeas() throws Exception {
        // Get the ideas
        restIdeasMockMvc.perform(get("/api/ideas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateIdeas() throws Exception {
        // Initialize the database
        ideasRepository.save(ideas);
        int databaseSizeBeforeUpdate = ideasRepository.findAll().size();

        // Update the ideas
        Ideas updatedIdeas = ideasRepository.findOne(ideas.getId());
        updatedIdeas
            .titre(UPDATED_TITRE)
            .genre(UPDATED_GENRE)
            .emotions(UPDATED_EMOTIONS)
            .phraseSynopsis(UPDATED_PHRASE_SYNOPSIS)
            .keywords(UPDATED_KEYWORDS)
            .roles(UPDATED_ROLES);
        IdeasDTO ideasDTO = ideasMapper.toDto(updatedIdeas);

        restIdeasMockMvc.perform(put("/api/ideas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ideasDTO)))
            .andExpect(status().isOk());

        // Validate the Ideas in the database
        List<Ideas> ideasList = ideasRepository.findAll();
        assertThat(ideasList).hasSize(databaseSizeBeforeUpdate);
        Ideas testIdeas = ideasList.get(ideasList.size() - 1);
        assertThat(testIdeas.getTitre()).isEqualTo(UPDATED_TITRE);
        assertThat(testIdeas.getGenre()).isEqualTo(UPDATED_GENRE);
        assertThat(testIdeas.getEmotions()).isEqualTo(UPDATED_EMOTIONS);
        assertThat(testIdeas.getPhraseSynopsis()).isEqualTo(UPDATED_PHRASE_SYNOPSIS);
        assertThat(testIdeas.getKeywords()).isEqualTo(UPDATED_KEYWORDS);
        assertThat(testIdeas.getRoles()).isEqualTo(UPDATED_ROLES);
    }

    @Test
    public void updateNonExistingIdeas() throws Exception {
        int databaseSizeBeforeUpdate = ideasRepository.findAll().size();

        // Create the Ideas
        IdeasDTO ideasDTO = ideasMapper.toDto(ideas);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restIdeasMockMvc.perform(put("/api/ideas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ideasDTO)))
            .andExpect(status().isCreated());

        // Validate the Ideas in the database
        List<Ideas> ideasList = ideasRepository.findAll();
        assertThat(ideasList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteIdeas() throws Exception {
        // Initialize the database
        ideasRepository.save(ideas);
        int databaseSizeBeforeDelete = ideasRepository.findAll().size();

        // Get the ideas
        restIdeasMockMvc.perform(delete("/api/ideas/{id}", ideas.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Ideas> ideasList = ideasRepository.findAll();
        assertThat(ideasList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Ideas.class);
        Ideas ideas1 = new Ideas();
        ideas1.setId("id1");
        Ideas ideas2 = new Ideas();
        ideas2.setId(ideas1.getId());
        assertThat(ideas1).isEqualTo(ideas2);
        ideas2.setId("id2");
        assertThat(ideas1).isNotEqualTo(ideas2);
        ideas1.setId(null);
        assertThat(ideas1).isNotEqualTo(ideas2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IdeasDTO.class);
        IdeasDTO ideasDTO1 = new IdeasDTO();
        ideasDTO1.setId("id1");
        IdeasDTO ideasDTO2 = new IdeasDTO();
        assertThat(ideasDTO1).isNotEqualTo(ideasDTO2);
        ideasDTO2.setId(ideasDTO1.getId());
        assertThat(ideasDTO1).isEqualTo(ideasDTO2);
        ideasDTO2.setId("id2");
        assertThat(ideasDTO1).isNotEqualTo(ideasDTO2);
        ideasDTO1.setId(null);
        assertThat(ideasDTO1).isNotEqualTo(ideasDTO2);
    }
}
